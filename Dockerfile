ARG PYTHON_BUILDER_IMAGE=3.11-slim-bullseye
ARG NODE_BUILDER_IMAGE=18.12-alpine
ARG GOOGLE_DISTROLESS_BASE_IMAGE=gcr.io/distroless/cc:nonroot


## --- Build the frontend files

FROM node:${NODE_BUILDER_IMAGE} as node-base

RUN npm install -g npm@latest --quiet

WORKDIR /app

COPY package*.json ./
RUN npm ci --quiet && npx browserslist@latest --update-db && npm cache clean --force --quiet

COPY tailwind.config.js rollup.config.js ./
COPY frontend/ ./frontend
RUN npm run build


## --- Build python and install packages

FROM python:${PYTHON_BUILDER_IMAGE} as python-base

ENV PATH="/home/nonroot/.local/bin:${PATH}" \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  PIP_NO_CACHE_DIR=1 \
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONFAULTHANDLER=1 \
  PYTHONHASHSEED=random \
  PIPENV_VENV_IN_PROJECT=1 \
  LANG=C.UTF-8 \
  LC_ALL=C.UTF-8

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /root/.cache \
  && rm -rf /var/apt/lists/* \
  && rm -rf /var/cache/apt/* \
  && apt-get purge -y --auto-remove

RUN groupadd nonroot && useradd -m nonroot -g nonroot
USER nonroot
WORKDIR /home/nonroot/

RUN pip install --upgrade pip pipenv

COPY Pipfile* ./
RUN pipenv install --deploy

COPY app/ ./app/
COPY --from=node-base /app/build ./app/static/build


## --- Distroless runtime build

FROM ${GOOGLE_DISTROLESS_BASE_IMAGE}

ARG CHIPSET_ARCH=x86_64-linux-gnu

# --- copy python itself from builder

COPY --from=python-base /usr/local/lib/ /usr/local/lib/
COPY --from=python-base /usr/local/bin/python /usr/local/bin/python
COPY --from=python-base /etc/ld.so.cache /etc/ld.so.cache

# --- add common compiled libraries

COPY --from=python-base /lib/${CHIPSET_ARCH}/libz.so.1 /lib/${CHIPSET_ARCH}/
COPY --from=python-base /usr/lib/${CHIPSET_ARCH}/libffi* /usr/lib/${CHIPSET_ARCH}/
COPY --from=python-base /lib/${CHIPSET_ARCH}/libexpat* /lib/${CHIPSET_ARCH}/

# --- add application files

COPY --from=python-base /home/nonroot/.venv /home/nonroot/.venv
COPY --from=python-base /home/nonroot/app/ /home/nonroot/app/

# --- standardise execution env

ENV PYTHONPATH=/home/nonroot/.venv/lib/python3.11/site-packages \
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONFAULTHANDLER=1 \
  LANG=C.UTF-8 \
  LC_ALL=C.UTF-8

STOPSIGNAL SIGINT
EXPOSE 5000/tcp

CMD ["python", "app/run.py", "-b 0.0.0.0:5000", "-w 3", "-t 60", "app:app"]
