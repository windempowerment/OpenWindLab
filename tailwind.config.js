const isProduction = !process.env.ROLLUP_WATCH;

module.exports = {
  mode: 'jit',
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  content: ['./frontend/**/*.svelte'],
  plugins: [require('@tailwindcss/forms')],
};
