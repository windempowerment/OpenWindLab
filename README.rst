OpenWindLab
===========

Small wind turbine data analysis tool ~ OpenWindLab, NTUA


Description
-----------

OpenWindLab is an open source tool developed by the Rural Electrification Research Group,
that calculates the performance characteristics of small wind turbines based on electrical
and meteorological measurements, according to IEC 61400-12-1. The power curve, the power
coefficient and the annual energy production are calculated and presented. The data can be
displayed in per second raw values or per minute averages. All data can be plotted for
different combinations of variables and custom made queries can be made to the data base
with regard to all data variables.


Usage
-----

An example of running the container is:

.. code:: console

  docker run --name openwindlab -p 5000:5000 registry.gitlab.com/rurerg/openwindlab:latest

You can pass environment variables to your container with the -e flag, ex. ``-e SECRET_KEY=1234``.
Or, if you don't want to have the value on the command-line where it will be displayed by ``docker ps``.
You can use an `env-file`_, ex. ``--env-file .env``. The available environment variables are listed below.

.. _`env-file`: https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file


Development
-----------

To run the project for development, run the following commands:

.. code:: console

  $ make build
  $ make up

Open http://localhost:5000 on your web browser and have fun :)


Environment variables
---------------------

``SECRET_KEY``
--------------

Default: ``''``

A secret key that will be used for securely signing the session cookie and can be used for any other
security related needs by extensions or your application. It should be a long random bytes or str.

``SQLALCHEMY_DATABASE_URI``
---------------------------

Default: ``''``

The database URI that should be used for the connection. SQLAlchemy indicates the source of an Engine
as a URI combined with optional keyword arguments to specify options for the Engine. The form of the
URI is:

``dialect+driver://username:password@host:port/database``

As for now, it has only been tested with a mssql database. For example:

``mssql+pymssql://USERNAME:PASSWORD@SERVER/DATABASE?charset=utf8``

``API_BASE_URL``
----------------

Default: ``'http://localhost:5000/api/'``

The location of the API in which the frontend will have to make calls.


License
-------

GNU General Public License v3.0
