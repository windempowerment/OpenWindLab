import os

from flask import Flask, render_template
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_filter import FlaskFilter
from flask_cors import CORS

from .config import config


app = Flask(__name__)
app.config.from_object(config)

api = Api(app, prefix="/api")
db = SQLAlchemy(app)
ma = Marshmallow(app)
filtr = FlaskFilter(app)

CORS(app, origins=os.getenv("CORS_ORIGINS", "http://localhost:5000"))


@app.route("/")
def index():
    api_base_url = os.getenv("API_BASE_URL", "http://localhost:5000/api/")
    return render_template("index.html", api_base_url=api_base_url)


from app import windturbines
from app import standard
from app import curve_comparison
from app import data_analysis
