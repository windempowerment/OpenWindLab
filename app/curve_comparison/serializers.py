from marshmallow import Schema, fields

from app.standard.serializers import (
    AnualEnergySchema,
    LamdaBinsSchema,
    PowerCurveSchema,
)


class ComparisonInputSchema(Schema):
    ids = fields.List(fields.Integer(), required=True)


comparison_input_schema = ComparisonInputSchema()
ac_voltage_schema = PowerCurveSchema(
    many=True, only=("bin", "avg_norm_voltage"))
aerodynamic_coef_schema = LamdaBinsSchema(
    many=True, only=("bin", "avg_aero_dynamic_coef"))
air_density_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_air_density"])
ambient_temp_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_ambient_temp"])
anual_energy_schema = AnualEnergySchema(many=True, only=["wind_speed", "aep"])
barometric_pressure_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_bar_pressure"])
furling_angle_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_furling_angle"])
power_coef_schema = PowerCurveSchema(many=True, only=["bin", "power_coef"])
power_curve_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_norm_power"])
relative_humidity_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_humidity"])
rotor_yaw_schema = PowerCurveSchema(many=True, only=["bin", "avg_rotor_yaw"])
stator_current_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_norm_current"])
stator_temp_schema = PowerCurveSchema(
    many=True, only=["bin", "avg_stator_temp"])
tip_speed_schema = PowerCurveSchema(many=True, only=["bin", "avg_lamda"])
