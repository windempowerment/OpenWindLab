from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from app import api
from app.windturbines.models import WindTurbine

from . import serializers as s


class BaseComparisonView(Resource):
    schema = None
    backref = None

    def parse_data(self, data):
        """
        A method to use, in case you want to parse the data before
        returning them throught the API.

        Here it simply returns the data without parsing it.
        """
        return data

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return {"message": "No input data provided"}, 400

        try:
            data = s.comparison_input_schema.load(json_data)
        except ValidationError as err:
            return err.messages, 422

        turbines = (
            WindTurbine.query.join(getattr(WindTurbine, self.backref.lower()))
            .filter(WindTurbine.id.in_(data["ids"]))
            .all()
        )

        res = {}
        for turbine in turbines:
            meas = getattr(turbine, self.backref.lower(), None)
            if meas is None:
                return {"message": "Backref does not exist"}, 500

            res[turbine.id] = self.schema.dump(self.parse_data(meas))

        return res, 200


@api.resource("/curve-comparison/ac-voltage/")
class AcVoltageComparisonView(BaseComparisonView):
    schema = s.ac_voltage_schema
    backref = "power_curve"


@api.resource("/curve-comparison/aerodynamic-coef/")
class AerodynamicCoefComparisonView(BaseComparisonView):
    schema = s.aerodynamic_coef_schema
    backref = "lamda_bins"

    def parse_data(self, data):
        # Keep only the wind speed bins with value under 9.5
        return filter(lambda m: m.bin <= 9, data)


@api.resource("/curve-comparison/air-density/")
class AirDensityComparisonView(BaseComparisonView):
    schema = s.air_density_schema
    backref = "power_curve"


@api.resource("/curve-comparison/ambient-temperature/")
class AmbientTemperatureComparisonView(BaseComparisonView):
    schema = s.ambient_temp_schema
    backref = "power_curve"


@api.resource("/curve-comparison/anual-energy/")
class AnualEnergyComparisonView(BaseComparisonView):
    schema = s.anual_energy_schema
    backref = "aep"


@api.resource("/curve-comparison/barometric-pressure/")
class BarometricPressureComparisonView(BaseComparisonView):
    schema = s.barometric_pressure_schema
    backref = "power_curve"


@api.resource("/curve-comparison/furling-angle/")
class FurlingAngleComparisonView(BaseComparisonView):
    schema = s.furling_angle_schema
    backref = "power_curve"


@api.resource("/curve-comparison/power-coef/")
class PowerCoefComparisonView(BaseComparisonView):
    schema = s.power_coef_schema
    backref = "power_curve"


@api.resource("/curve-comparison/power-curve/")
class PowerCurveComparisonView(BaseComparisonView):
    schema = s.power_curve_schema
    backref = "power_curve"


@api.resource("/curve-comparison/relative-humidity/")
class RelativeHumidityComparisonView(BaseComparisonView):
    schema = s.relative_humidity_schema
    backref = "power_curve"


@api.resource("/curve-comparison/rotor-yaw/")
class RotorYawComparisonView(BaseComparisonView):
    schema = s.rotor_yaw_schema
    backref = "power_curve"


@api.resource("/curve-comparison/stator-current/")
class StatorCurrentComparisonView(BaseComparisonView):
    schema = s.stator_current_schema
    backref = "power_curve"


@api.resource("/curve-comparison/stator-temperature/")
class StatorTemperatureComparisonView(BaseComparisonView):
    schema = s.stator_temp_schema
    backref = "power_curve"


@api.resource("/curve-comparison/tip-speed-ratio/")
class TipSpeedomparisonView(BaseComparisonView):
    schema = s.tip_speed_schema
    backref = "power_curve"
