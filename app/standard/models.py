from app import db

from sqlalchemy.orm import declared_attr


class WindTurbineMixin:
    @declared_attr
    def turbine_id(cls):
        return db.Column(
            "WT_ID",
            db.SmallInteger,
            db.ForeignKey("WindTurbine.WT_ID"),
            primary_key=True,
        )


# NOTE This table has a different foreign key.
# So you can't use the WindturbineMixin here.
class Measurement(WindTurbineMixin, db.Model):
    __tablename__ = "Measurement"

    meas_timestamp = db.Column("MeasTimeStamp", db.DateTime, primary_key=True)
    bar_pressure = db.Column("BarPressure", db.Float)
    current_r = db.Column("CurrentR", db.Float)
    current_s = db.Column("CurrentS", db.Float)
    current_t = db.Column("CurrentT", db.Float)
    furling_angle_voltage = db.Column("FurlingAngleVoltage", db.Float)
    grid_power_ac = db.Column("GridPowerAC", db.Float)
    humidity = db.Column("Humidity", db.Float)
    power_ac = db.Column("PowerAC", db.Float)
    rpm = db.Column("RPM", db.Float)
    stator_temp = db.Column("StatorTemp", db.Float)
    temperature = db.Column("Temperature", db.Float)
    voltage_r = db.Column("VoltageR", db.Float)
    voltage_s = db.Column("VoltageS", db.Float)
    voltage_t = db.Column("VoltageT", db.Float)
    wind_direction = db.Column("WindDirection", db.Float)
    validation_wind_speed = db.Column("ValidationWindSpeed", db.Float)
    wind_speed = db.Column("WindSpeed", db.Float)
    yaw_angle_voltage = db.Column("YawAngleVoltage", db.Float)

    turbine = db.relationship(
        "WindTurbine", backref=db.backref("measurement", lazy=True)
    )

    def __repr__(self):
        return f"<Measurement {self.meas_timestamp} for turbine #{self.turbine_id}>"

    def date():
        return Measurement.meas_timestamp


class ProcessedSecond(WindTurbineMixin, db.Model):
    meas_timestamp = db.Column("MeasTimeStamp", db.DateTime, primary_key=True)
    air_density = db.Column("AirDensity", db.Float)
    bar_pressure = db.Column("BarPressure", db.Float)
    furling_angle = db.Column("FurlingAngle", db.Float)
    humidity = db.Column("Humidity", db.Float)
    mean_current = db.Column("MeanCurrent", db.Float)
    mean_voltage = db.Column("MeanVoltage", db.Float)
    power_sec = db.Column("PowerSec", db.Float)
    rpm = db.Column("RPM", db.Float)
    temperature = db.Column("Temperature", db.Float)
    validation_wind_speed = db.Column("ValidationWindSpeed", db.Float)
    vapour_pressure = db.Column("VapourPressure", db.Float)
    voltage_dc = db.Column("VoltageDC", db.Float)
    wind_direction = db.Column("WindDirection", db.Float)
    wind_sector = db.Column("WindSector", db.Float)
    wind_speed = db.Column("WindSpeed", db.Float)
    yaw_angle = db.Column("YawAngle", db.Float)

    def __repr__(self):
        return f"<Processed {self.meas_timestamp} for turbine #{self.turbine_id}>"

    def date():
        return ProcessedSecond.meas_timestamp


class PerMinutes(WindTurbineMixin, db.Model):
    __tablename__ = "PerMinutes"

    meas_datetime = db.Column("MeasDateTime", db.DateTime, primary_key=True)
    avg_air_density = db.Column("AvgAirDensity", db.Float)
    avg_current = db.Column("AvgCurrent", db.Float)
    avg_current_samples = db.Column("AvgCurrentSamples", db.Integer)
    avg_furling_angle = db.Column("AvgFurlingAngle", db.Float)
    avg_mean_voltage = db.Column("AvgMeanVoltage", db.Float)
    avg_power = db.Column("AvgPower", db.Float)
    avg_power_norm = db.Column("AvgPower_Norm", db.Float)
    avg_power_norm_sea_level = db.Column("AvgPower_NormSeaLevel", db.Float)
    avg_stator_temp = db.Column("AvgStatorTemp", db.Float)
    avg_ambient_temp = db.Column("AvgAmbientTemp", db.Float)
    avg_humidity = db.Column("AvgHumidity", db.Float)
    avg_bar_pressure = db.Column("AvgBarPressure", db.Float)
    avg_rotor_yaw = db.Column("AvgRotorYaw", db.Float)
    avg_rpm = db.Column("AvgRPM", db.Float)
    avg_vapour_pressure = db.Column("AvgVapourPressure", db.Float)
    avg_voltage_samples = db.Column("AvgVoltageSamples", db.Integer)
    avg_wind_direction = db.Column("AvgWindDirection", db.Float)
    avg_wind_speed = db.Column("AvgWindSpeed", db.Float)
    avg_wind_speed_norm = db.Column("AvgWindSpeed_Norm", db.Float)
    avg_wind_speed_norm_sea_level = db.Column("AvgWindSpeed_NormSeaLevel", db.Float)
    avg_yaw_angle = db.Column("AvgYawAngle", db.Float)
    cp = db.Column("Cp", db.Float)
    lamda = db.Column("Lamda", db.Float)
    max_power = db.Column("MaxPower", db.Float)
    max_power_norm = db.Column("MaxPower_Norm", db.Float)
    max_power_norm_sea_level = db.Column("MaxPower_NormSeaLevel", db.Float)
    min_power = db.Column("MinPower", db.Float)
    min_power_norm = db.Column("MinPower_Norm", db.Float)
    min_power_norm_sea_level = db.Column("MinPower_NormSeaLevel", db.Float)
    power_deviation = db.Column("PowerDeviation", db.Float)
    power_deviation_norm = db.Column("PowerDeviation_Norm", db.Float)
    power_deviation_norm_sea_level = db.Column("PowerDeviation_NormSeaLevel", db.Float)
    pw = db.Column("Pw", db.Float)
    rad_speed = db.Column("RadSpeed", db.Float)
    torque = db.Column("Torque", db.Float)

    turbine = db.relationship(
        "WindTurbine", backref=db.backref("per_minutes", lazy=True)
    )

    def __repr__(self):
        return f"<Per minutes {self.meas_datetime} for turbine #{self.turbine_id}>"

    def date():
        return PerMinutes.meas_datetime


# NOTE WindturbineMixin can't be used here, this table has a different foreign key.
class PowerCurve(db.Model):
    __tablename__ = "PowerCurve"

    bin = db.Column("bin", db.Float, primary_key=True)
    bin_samples = db.Column("binSamples", db.Integer)
    avg_humidity = db.Column("AvgHumidity", db.Float)
    avg_air_density = db.Column("AvgAirDensity", db.Float)
    avg_ambient_temp = db.Column("AvgAmbientTemp", db.Float)
    avg_furling_angle = db.Column("AvgFurlingAngle", db.Float)
    avg_lamda = db.Column("AvgLamda", db.Float)
    avg_norm_power = db.Column("AvgNormPower", db.Float)
    avg_norm_current = db.Column("AvgNormCurrent", db.Float)
    avg_norm_voltage = db.Column("AvgNormVoltage", db.Float)
    avg_norm_wind_speed = db.Column("AvgNormWindSpeed", db.Float)
    avg_rotor_yaw = db.Column("AvgRotorYaw", db.Float)
    avg_stator_temp = db.Column("AvgStatorTemp", db.Float)
    avg_yaw_angle = db.Column("AvgYawAngle", db.Float)
    avg_bar_pressure = db.Column("AvgBarPressure", db.Float)
    max_avg_norm_power = db.Column("MaxAvgNormPower", db.Float)
    min_avg_norm_power = db.Column("MinAvgNormPower", db.Float)
    power_coef = db.Column("PowerCoef", db.Float)
    si = db.Column("Si", db.Float)
    ui = db.Column("Ui", db.Float)
    uc = db.Column("Uc", db.Float)

    turbine_id = db.Column(
        "WTID",
        db.SmallInteger,
        db.ForeignKey("WindTurbine.WT_ID"),
        primary_key=True,
    )
    turbine = db.relationship(
        "WindTurbine", backref=db.backref("power_curve", lazy=True)
    )

    def __repr__(self):
        return f"<Power curve {self.bin} for turbine #{self.turbine_id}>"


# NOTE WindturbineMixin can't be used here, this table has a different foreign key.
class AnualEnergy(db.Model):
    __tablename__ = "AEP"

    wind_speed = db.Column("WindSpeed", db.Integer, primary_key=True)
    aep = db.Column("AEP", db.Float)
    percentage = db.Column("Percentage", db.Float)
    uaep = db.Column("Uaep", db.Float)

    turbine_id = db.Column(
        "WTID",
        db.SmallInteger,
        db.ForeignKey("WindTurbine.WT_ID"),
        primary_key=True,
    )
    turbine = db.relationship("WindTurbine", backref=db.backref("aep", lazy=True))

    def __repr__(self):
        return f"<Wind speed {self.wind_speed} for turbine #{self.turbine_id}>"


# NOTE WindturbineMixin can't be used here, this table has a different foreign key.
class LamdaBins(db.Model):
    __tablename__ = "LambaBins"

    bin = db.Column("bin", db.Float, primary_key=True)
    bin_samples = db.Column("bin_Samples", db.Integer)
    bin_frequency = db.Column("bin_Frequency", db.Float)
    avg_actual_meas_wind_speed = db.Column("AvgActualMeasWindSpeed", db.Float)
    avg_actual_power = db.Column("AvgActualPower", db.Float)
    avg_aero_dynamic_coef = db.Column("AvgAeroDynamicCoef", db.Float)

    turbine_id = db.Column(
        "WTID",
        db.SmallInteger,
        db.ForeignKey("WindTurbine.WT_ID"),
        primary_key=True,
    )
    turbine = db.relationship(
        "WindTurbine", backref=db.backref("lamda_bins", lazy=True)
    )

    def __repr__(self):
        return f"<Lamda bins {self.bin} for turbine #{self.turbine_id}>"


class ShadowingRange(WindTurbineMixin, db.Model):
    __tablename__ = "ShadowingRange"

    range_end = db.Column("RangeEnd", db.Float)
    range_start = db.Column("RangeStart", db.Float)

    turbine = db.relationship(
        "WindTurbine", backref=db.backref("shadowing_range", lazy=True)
    )

    def __repr__(self):
        return f"<Shadowing range {self.range_start} - {self.range_end} for turbine #{self.turbine_id}>"
