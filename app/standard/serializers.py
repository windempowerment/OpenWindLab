from app import ma

from .models import (
    Measurement,
    ProcessedSecond,
    PerMinutes,
    PowerCurve,
    AnualEnergy,
    LamdaBins,
    ShadowingRange,
)


class MeasurementSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Measurement
        ordered = True


class ProcessedSecondSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ProcessedSecond
        ordered = True


class PerMinutesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = PerMinutes
        ordered = True


class PowerCurveSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = PowerCurve
        ordered = True


class AnualEnergySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = AnualEnergy
        ordered = True


class LamdaBinsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = LamdaBins
        ordered = True


class ShadowingRangeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ShadowingRange
        ordered = True


measurement_schema = MeasurementSchema(many=True)
processed_second_schema = ProcessedSecondSchema(many=True)
per_minutes_schema = PerMinutesSchema(many=True)
power_curve_schema = PowerCurveSchema(many=True)
anual_energy_schema = AnualEnergySchema(many=True)
lamda_bins_schema = LamdaBinsSchema(many=True)
shadowing_range_schema = ShadowingRangeSchema(many=True)
