from flask import request, abort
from flask_filter.schemas import FilterSchema
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy import select
from sqlalchemy.sql import column, func

from app import api, db
from app.windturbines.models import WindTurbine
from app.windturbines.serializers import turbine_schema

from . import models as m
from . import serializers as s


class BasePostView(Resource):
    schema = None
    model = None

    def get_data(self, turbine: WindTurbine, filters):
        # Initialize the query, by filtering data for the selected windturbine
        query = self.model.query.filter(self.model.turbine_id == turbine.id)

        # Apply the filters that the user has requested
        for f in filters:
            query = f.apply(query, self.model)

        # Order by the model's captured datetime and paginate the results
        query = query.order_by(self.model.date()).paginate(per_page=100)

        data = {
            "raw_data": self.schema.dump(query.items),
            "pagination": {
                "page": query.page,
                "pages": query.pages,
                "per_page": query.per_page,
                "total": query.total,
            },
        }
        return data

    def post(self, id: int):
        # Get the windturbine base on the id in the URL
        turbine = WindTurbine.query.get(id)
        if not turbine:
            return {"message": "Windturbine does not exist"}, 404

        json_data = request.get_json(force=True)

        try:
            filters = FilterSchema(many=True).load(json_data)
        except ValidationError:
            return {"message": "One of your filters is invalid"}, 400

        # If the selected filter fields are not actual model fields, return an error message
        if not all(map(lambda f: getattr(self.model, f.field, None), filters)):
            return {
                "message": "Filter fields do not correspond to model attributes"
            }, 400

        data = self.get_data(turbine, filters)
        return data, 200


@api.resource("/windturbines/<int:id>/processed-seconds/")
class ProcessedSecondView(BasePostView):
    schema = s.processed_second_schema
    model = m.ProcessedSecond

    def get_data(self, turbine: WindTurbine, filters):
        # Get the current page for which we need the data
        try:
            page = int(request.args.get("page", 1))
        except (TypeError, ValueError):
            abort(404)

        query = (
            select(
                column("MeasTimeStamp").label("meas_timestamp"),
                column("MeanCurrent").label("mean_current"),
                column("VapourPressure").label("vapour_pressure"),
                column("MeanVoltage").label("mean_voltage"),
                column("Temperature").label("temperature"),
                column("Humidity").label("humidity"),
                column("BarPressure").label("bar_pressure"),
                column("RPM").label("rpm"),
                column("WindSector").label("wind_sector"),
                column("WindSpeed").label("wind_speed"),
                column("AirDensity").label("air_density"),
                column("VoltageDC").label("voltage_dc"),
                column("PowerSec").label("power_sec"),
            )
            .select_from(
                func.udf_MeanPerSecond(
                    turbine.id, turbine.meas_start, turbine.meas_end, 1
                )
            )
            .order_by(column("MeasTimeStamp"))
            .limit(100)
            .offset((page - 1) * 100)
        )

        for f in filters:
            field = getattr(self.model, f.field)
            if f.OP == "<":
                query = query.filter(column(field.name) < float(f.value))
            elif f.OP == ">":
                query = query.filter(column(field.name) > float(f.value))
            elif f.OP == "=":
                query = query.filter(column(field.name) == int(f.value))

        query_data = db.session.execute(query)
        data = {
            "raw_data": self.schema.dump(query_data),
            "pagination": {
                "page": page,
                "pages": page + 1,
                "per_page": query_data.rowcount,
                # TODO: This should be aaded if the total count can be efficiently calculated
                #  "total": query.total,
            },
        }
        return data


@api.resource("/windturbines/<int:id>/measurements/")
class MeasurementView(BasePostView):
    schema = s.measurement_schema
    model = m.Measurement


@api.resource("/windturbines/<int:id>/per-minutes/")
class PerMinutesView(BasePostView):
    schema = s.per_minutes_schema
    model = m.PerMinutes


class BaseGetView(Resource):
    schema = None
    model = None

    def get_query(self, **kwargs):
        # Get the graph data for the requested windturbine
        query = self.model.query.filter(self.model.turbine_id == kwargs["id"])
        return query

    def get(self, **kwargs):
        # Get the windturbine base on the id in the URL
        turbine = WindTurbine.query.get(kwargs["id"])
        if not turbine:
            return {"message": "Windturbine does not exist"}, 404

        data = {
            "windturbine": turbine_schema.dump(turbine),
            "raw_data": self.schema.dump(self.get_query(**kwargs)),
        }
        return data, 200


@api.resource("/windturbines/<int:id>/power-curve/")
class PowerCurveView(BaseGetView):
    schema = s.power_curve_schema
    model = m.PowerCurve


@api.resource("/windturbines/<int:id>/anual-energy/")
class AnualEnergyView(BaseGetView):
    schema = s.anual_energy_schema
    model = m.AnualEnergy


@api.resource("/windturbines/<int:id>/lamda-bins/")
class LamdaBinsView(BaseGetView):
    schema = s.lamda_bins_schema
    model = m.LamdaBins


@api.resource("/windturbines/<int:id>/lamda-bins/<float:bin>/")
class LamdaBinsPerBinView(BaseGetView):
    schema = s.lamda_bins_schema

    def get_query(self, **kwargs):
        query = (
            select(
                column("bin"),
                column("bin_Samples").label("bin_samples"),
                column("bin_Frequency").label("bin_frequency"),
                column("AvgActualMeasWindSpeed").label("avg_actual_meas_wind_speed"),
                column("AvgActualPower").label("avg_actual_power"),
                column("AvgAeroDynamicCoef").label("avg_aero_dynamic_coef"),
            )
            .select_from(
                func.udf_BinByLamdaOnWindSpeedTable(kwargs["id"], 0.5, 9, kwargs["bin"])
            )
            .order_by(column("bin"))
        )
        return db.session.execute(query)


@api.resource("/windturbines/<int:id>/shadowing-range/")
class ShadowingRangeView(BaseGetView):
    schema = s.shadowing_range_schema
    model = m.ShadowingRange
