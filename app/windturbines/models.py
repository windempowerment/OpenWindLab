from app import db


class WindTurbine(db.Model):
    __tablename__ = "WindTurbine"

    id = db.Column("WT_ID", db.SmallInteger, primary_key=True)
    name = db.Column("WT_Name", db.String(100))
    description = db.Column("WT_Description", db.String(255))
    location = db.Column("Location", db.String(50))
    resistance = db.Column("Resistance", db.Float)
    zero_current_margin = db.Column("ZeroCurrentMargin", db.Float)
    radius = db.Column("Radius", db.Float)
    meas_start = db.Column("Meas_Start", db.DateTime)
    meas_end = db.Column("Meas_End", db.DateTime)
    furling_a = db.Column("Furling_a", db.Float)
    furling_b = db.Column("Furling_b", db.Float)
    yaw_a = db.Column("Yaw_a", db.Float)
    yaw_b = db.Column("Yaw_b", db.Float)
    torque_a = db.Column("Torque_a", db.Float)
    torque_b = db.Column("Torque_b", db.Float)
    is_public = db.Column("Is_Public", db.Boolean)

    def __repr__(self):
        return f"<WindTurbine {self.name}>"
