from app import ma

from .models import WindTurbine


class WindTurbineSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = WindTurbine
        ordered = True


turbine_schema = WindTurbineSchema()
turbines_schema = WindTurbineSchema(many=True)
