from flask import request
from flask_restful import Resource
from sqlalchemy.sql.expression import true

from app import api

from .models import WindTurbine
from .serializers import turbine_schema, turbines_schema


@api.resource("/windturbines/<int:id>/")
class WindTurbineView(Resource):
    def get(self, id: int):
        turbine = WindTurbine.query.get(id)
        if not turbine:
            return {"message": "Windturbine does not exist"}, 404

        data = turbine_schema.dump(turbine)
        return data, 200


@api.resource("/windturbines/")
class WindTurbineListView(Resource):
    def get(self):
        try:
            from_public_domain = request.args.get("is_public", "true").lower() == "true"
        except (TypeError, ValueError):
            from_public_domain = False

        if from_public_domain:
            turbines = WindTurbine.query.filter(WindTurbine.is_public == true())
        else:
            turbines = WindTurbine.query.all()

        data = turbines_schema.dump(turbines)
        return data, 200
