from marshmallow import Schema, fields


class DataAnalysisSchema(Schema):
    id = fields.Integer(required=True)
    x_axis = fields.Str(required=True)
    y_axis = fields.Str(required=True)
    y2_axis = fields.Str()
    start_date = fields.DateTime(required=True)
    end_date = fields.DateTime(required=True)


data_analysis_schema = DataAnalysisSchema()
