from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy import select
from sqlalchemy.orm import load_only
from sqlalchemy.sql import column, func

from app import api, db
from app.standard.models import PerMinutes, ProcessedSecond
from app.standard.serializers import PerMinutesSchema, ProcessedSecondSchema

from .serializers import data_analysis_schema


class BaseTimeAnalysisView(Resource):
    schema = None
    model = None

    def get_query(self, data, axis_keys: list[str]):
        # Initialize the query by selecting only the fields corresponding to the selected axis,
        # filter only the data for the selected windturbine and keep the data between the
        # selected datetimes the user has requested. Order by the model's captured datetime.
        query = (
            self.model.query.options(load_only(*[getattr(self.model, key) for key in axis_keys]))
            .filter(self.model.turbine_id == data["id"])
            .filter(self.model.date().between(data["start_date"], data["end_date"]))
            .order_by(self.model.date())
        )
        return query

    def post(self):
        json_data = request.get_json(force=True)

        try:
            data = data_analysis_schema.load(json_data)
        except ValidationError as err:
            return err.messages, 422

        # Get the requested axis fields from the POST data
        x_axis = data["x_axis"]
        y_axis = data["y_axis"]
        y2_axis = data["y2_axis"]
        axis_keys = [x_axis, y_axis]
        if y2_axis:
            axis_keys.append(y2_axis)

        # If the selected axis values are not actual model fields, return an error message
        if not all(map(lambda name: getattr(self.model, name, None), axis_keys)):
            return {"message": "Axis keys do not correspond to model attributes"}, 400

        # Serialize the query data and keep only the requested axis fields
        raw_data = self.schema(many=True, only=axis_keys).dump(
            self.get_query(data, axis_keys)
        )

        data = {
            "raw_data": raw_data,
        }
        return data, 200


@api.resource("/data-analysis/per-second/")
class PerSecondAnalysisView(BaseTimeAnalysisView):
    schema = ProcessedSecondSchema
    model = ProcessedSecond

    def get_query(self, data, axis_keys: list[str]):
        columns = []
        for axis in axis_keys:
            field = getattr(self.model, axis)
            columns.append(column(field.name).label(axis))

        query = (
            select(*columns)
            .select_from(
                func.udf_MeanPerSecondForUI(
                    data["id"], data["start_date"], data["end_date"], 0
                )
            )
            .order_by(column("MeasTimeStamp"))
        )
        return db.session.execute(query)


@api.resource("/data-analysis/per-minute/")
class PerMinuteAnalysisView(BaseTimeAnalysisView):
    schema = PerMinutesSchema
    model = PerMinutes
