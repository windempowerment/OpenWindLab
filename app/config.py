import os


class Config:
    THREADS_PER_PAGE = 2
    SECRET_KEY = os.getenv("SECRET_KEY")

    # Enable protection against CSRF
    CSRF_SESSION_KEY = SECRET_KEY
    CSRF_ENABLED = True

    # Sql Alchemy
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")


class ProductionConfig(Config):
    pass


config = ProductionConfig()

