import Chart from 'chart.js/auto';
import 'chartjs-adapter-date-fns';
import zoomPlugin from 'chartjs-plugin-zoom';
import { LineWithErrorBarsController } from 'chartjs-chart-error-bars';

// Register controller in chart.js and ensure the defaults are set
Chart.register(LineWithErrorBarsController, zoomPlugin);

// Our basic colors used in the charts
// Primary color is blue and secondary is red, both with opacity
const primaryColor = 'rgba(59, 130, 246, 0.5)';
const secondaryColor = 'rgb(236, 72, 106, 0.5)';
const primaryColorHex = '#3B82F6';
const secondaryColorHex = '#EC486A';

// A function to get some colors mainly used in curve comparison
// in order to color the windturbine's curves differently
function getColor(index) {
  const colors = [
    primaryColorHex,
    secondaryColorHex,
    '#FFC864',
    '#A7AABC',
    '#27C196',
    '#434656',
    '#F9F871',
    '#AE003C',
    '#6FFACC',
  ];

  return colors[index % colors.length];
}

// This function is used to prepare the data and the chart configuration
// and is used in all the standard graph layouts to draw the chart
// It takes the windturbine ID, the graph data as returned from the api
// and the options for the specific standard graph
export function initStandardChart(id, graphData, options) {
  let labels = graphData.map((d) => d[options.xAxisKey]);
  let data = graphData.map((d) => d[options.yAxisKey]);

  const withErrorBars = options.yAxisKey === 'avg_norm_power';
  if (withErrorBars) {
    // If the graph we are displaying is the Power Curve
    // then also add the uncertainty and the bin samples to the data
    data = graphData.map((d) => ({
      y: d[options.yAxisKey],
      yMin: d[options.yAxisKey] - d.uc,
      yMax: d[options.yAxisKey] + d.uc,
      samples: d.bin_samples,
      uncertainty: d.uc,
    }));
  } else if (options.yAxisKey === 'avg_aero_dynamic_coef') {
    // If the graph we are displaying is the Aerodynamic coefficient
    // keep the wind speed bins with value under 9
    labels = labels.filter((x) => x <= 9);
    data = data.slice(0, labels.length);
  }

  // Initialize the current chart options
  const chartOptions = {
    responsive: true,
    interaction: {
      mode: 'index',
      intersect: false,
    },
    elements: {
      line: {
        tension: 0.4,
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: options.xTitle,
        },
      },
      y: {
        title: {
          display: true,
          text: options.yTitle,
        },
        beginAtZero: true,
      },
    },
    plugins: {
      tooltip: {
        callbacks: {
          // Add custom title to the graph with the x axis units
          title: (tooltipItems) => {
            return options.xTitle + ': ' + tooltipItems[0].label;
          },
          // Include the y axis units to the tooltip labels
          label: (tooltipItem) => {
            const label = tooltipItem.dataset.label;
            const data = tooltipItem.parsed.y.toFixed(2);

            let msg = `${label}: ${data}`;
            if (options.yUnits) {
              msg += ` (${options.yUnits})`;
            }
            return msg;
          },
        },
      },
    },
  };

  // If the graph we are displaying is the Power Curve graph, then
  // add to the chart setup the error bars and change the tooltip
  // to include the uncertainty value and the total bin samples
  if (withErrorBars) {
    chartOptions.errorBarLineWidth = 2;
    chartOptions.errorBarWhiskerLineWidth = 2;
    chartOptions.errorBarColor = secondaryColor;
    chartOptions.errorBarWhiskerColor = secondaryColor;

    chartOptions.plugins.tooltip.callbacks.label = (tooltipItem) => {
      const label = tooltipItem.dataset.label;
      const data = tooltipItem.raw.y.toFixed(2);
      const uncertainty = tooltipItem.raw.uncertainty.toFixed(2);
      return `${label}: ${data} ± ${uncertainty} (${options.yUnits})`;
    };

    chartOptions.plugins.tooltip.callbacks.footer = (tooltipItems) => {
      return 'Samples: ' + tooltipItems[0].raw.samples;
    };
  }

  let chartType = 'line';
  if (withErrorBars) {
    chartType = 'lineWithErrorBars';
  } else if (options.route === '/graphs/aerodynamic-coefficient-per-bin/') {
    chartType = 'scatter';
  }

  // Setup the chart config with labels, datasets and options
  const config = {
    type: chartType,
    data: {
      labels: labels,
      datasets: [
        {
          label: `SWT${id.toString().padStart(2, '0')}`,
          data: data,
          borderColor: primaryColor,
          backgroundColor: primaryColor,
        },
      ],
    },
    options: chartOptions,
  };

  // Get the canvas element and initialize the chart
  const ctx = document.getElementById('graph');
  const chart = new Chart(ctx, config);
  return chart;
}

// This function is used to prepare the data and the chart configuration
// and is used in curve comparison layout to draw the chart
// It takes the graph data as returned from the api
// and the selected standard graph the user has chosen
export function initCurveComparisonChart(graphData, selectedGraph) {
  let index = 0;
  let labels = new Set();
  let datasets = [];

  // Loop over the graph data of each windturbine
  for (let key in graphData) {
    // Create unique label values by adding them to the labels set
    graphData[key].map((d) => {
      labels.add(d[selectedGraph.xAxisKey]);
    });

    // Get a new color based on the index
    const color = getColor(index++);

    datasets.push({
      label: `SWT${key.padStart(2, '0')}`,
      data: graphData[key],
      borderColor: color,
      backgroundColor: color,
    });
  }

  // Initialize the current chart options
  const chartOptions = {
    responsive: true,
    interaction: {
      mode: 'x',
      intersect: false,
    },
    elements: {
      line: {
        tension: 0.4,
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: selectedGraph.xTitle,
        },
      },
      y: {
        title: {
          display: true,
          text: selectedGraph.yTitle,
        },
        beginAtZero: true,
      },
    },
    parsing: {
      xAxisKey: selectedGraph.xAxisKey,
      yAxisKey: selectedGraph.yAxisKey,
    },
    plugins: {
      tooltip: {
        callbacks: {
          // Add custom title to the graph with the x axis units
          title: (tooltipItems) => {
            return selectedGraph.xTitle + ': ' + tooltipItems[0].label;
          },
          // Include the y axis units to the tooltip labels
          label: (tooltipItem) => {
            const label = tooltipItem.dataset.label;
            const data = tooltipItem.parsed.y.toFixed(2);

            let msg = `${label}: ${data}`;
            if (selectedGraph.yUnits) {
              msg += ` (${selectedGraph.yUnits})`;
            }
            return msg;
          },
        },
      },
    },
  };

  // Setup the chart config with labels, datasets and options
  const config = {
    type: 'line',
    data: {
      // Create an array from the labels set and sort them in ascending order
      labels: Array.from(labels).sort((a, b) => a - b),
      datasets: datasets,
    },
    options: chartOptions,
  };

  // Get the canvas element and initialize the chart
  const ctx = document.getElementById('graph');
  const chart = new Chart(ctx, config);
  return chart;
}

export function initDataAnalysisChart(graphData, options) {
  const labels = graphData.map((d) => d[options.xAxis.key]);
  const yData = graphData.map((d) => d[options.yAxis.key]);

  // Initialize the current chart options
  let chartOptions = {
    responsive: true,
    animation: false,
    interaction: {
      mode: 'index',
      intersect: false,
    },
    elements: {
      line: {
        tension: 0.4,
      },
    },
    scales: {
      x: {
        type: 'time',
        ticks: {
          autoSkip: true,
          autoSkipPadding: 50,
          maxRotation: 0,
        },
        time: {
          displayFormats: {
            hour: 'HH:mm',
            minute: 'HH:mm',
            second: 'HH:mm:ss',
          },
        },
        title: {
          display: true,
          text: options.xTitle,
        },
      },
      y: {
        type: 'linear',
        position: 'left',
        ticks: {
          color: primaryColorHex,
        },
        title: {
          display: true,
          text: options.yAxis.title,
        },
      },
    },
    plugins: {
      zoom: {
        pan: {
          enabled: true,
          modifierKey: 'ctrl',
          mode: 'x',
        },
        zoom: {
          wheel: {
            enabled: true,
          },
          drag: {
            enabled: true,
          },
          pinch: {
            enabled: true,
          },
          mode: 'x',
        },
      },
    },
  };

  // Setup the chart config with labels, datasets and options
  const config = {
    type: options.yChartType,
    data: {
      labels: labels,
      datasets: [
        {
          label: options.yAxis.title,
          data: yData,
          borderColor: primaryColor,
          backgroundColor: primaryColor,
          type: options.yChartType,
          yAxisID: 'y',
          radius: options.yChartType === 'scatter' ? 3 : 0,
        },
      ],
    },
    options: chartOptions,
  };

  // If the user has selected a second axis to display then
  // add the new scale to the options and add the second data
  // to the datasets. Use the secondary color for the second axis
  // in order to differentiate the graphs
  if (options.y2Axis) {
    chartOptions.scales.y2 = {
      type: 'linear',
      position: 'right',
      ticks: {
        color: secondaryColorHex,
      },
      title: {
        display: true,
        text: options.y2Axis.title,
      },
      grid: {
        drawOnChartArea: false,
      },
    };

    const y2Data = graphData.map((d) => d[options.y2Axis.key]);

    config.data.datasets.push({
      label: options.y2Axis.title,
      data: y2Data,
      borderColor: secondaryColor,
      backgroundColor: secondaryColor,
      type: options.y2ChartType,
      yAxisID: 'y2',
      radius: options.y2ChartType === 'scatter' ? 3 : 0,
    });
  }

  // Get the canvas element and initialize the chart
  const ctx = document.getElementById('graph');
  const chart = new Chart(ctx, config);
  return chart;
}
