import { writable } from 'svelte/store';

export const headerTitle = writable('Dashboard');

let notificationTimeoutID;

export const notification = writable(null);

export function notify(message, type = 'info', timeout = 5000) {
  if (notificationTimeoutID) {
    clearTimeout(notificationTimeoutID);
  }

  notification.set({
    message: message,
    type: type,
  });

  notificationTimeoutID = setTimeout(() => {
    notification.set(null);
  }, timeout);
}
