import Home from './routes/Home.svelte';
import About from './routes/About.svelte';
import NotFound from './routes/NotFound.svelte';
import WindTurbineView from './routes/windturbines/View.svelte';

import CurveComparison from './routes/CurveComparison.svelte';
import AcVoltage from './routes/graphs/AcVoltage.svelte';
import AerodynamicCoef from './routes/graphs/AerodynamicCoef.svelte';
import AerodynamicCoefPerBin from './routes/graphs/AerodynamicCoefPerBin.svelte';
import AirDensity from './routes/graphs/AirDensity.svelte';
import AmbientTemperature from './routes/graphs/AmbientTemperature.svelte';
import AnualEnergy from './routes/graphs/AnualEnergy.svelte';
import BarometricPressure from './routes/graphs/BarometricPressure.svelte';
import FurlingAngle from './routes/graphs/FurlingAngle.svelte';
import PowerCoef from './routes/graphs/PowerCoef.svelte';
import PowerCurve from './routes/graphs/PowerCurve.svelte';
import RelativeHumidity from './routes/graphs/RelativeHumidity.svelte';
import RotorYaw from './routes/graphs/RotorYaw.svelte';
import StatorCurrent from './routes/graphs/StatorCurrent.svelte';
import StatorTemperature from './routes/graphs/StatorTemperature.svelte';
import TipSpeedRatio from './routes/graphs/TipSpeedRatio.svelte';

import PerSecond from './routes/data_analysis/PerSecond.svelte';
import PerMinute from './routes/data_analysis/PerMinute.svelte';
import Query from './routes/data_analysis/Query.svelte';

const routes = {
  '/': Home,
  '/about/': About,

  '/windturbines/:id/': WindTurbineView,

  '/graphs/ac-voltage/:id?/': AcVoltage,
  '/graphs/aerodynamic-coefficient/:id?/': AerodynamicCoef,
  '/graphs/aerodynamic-coefficient-per-bin/:id?/': AerodynamicCoefPerBin,
  '/graphs/air-density/:id?/': AirDensity,
  '/graphs/ambient-temperature/:id?/': AmbientTemperature,
  '/graphs/anual-energy-production/:id?/': AnualEnergy,
  '/graphs/barometric-pressure/:id?/': BarometricPressure,
  '/graphs/furling-angle/:id?/': FurlingAngle,
  '/graphs/power-coefficient/:id?/': PowerCoef,
  '/graphs/power-curve/:id?/': PowerCurve,
  '/graphs/relative-humidity/:id?/': RelativeHumidity,
  '/graphs/rotor-yaw/:id?/': RotorYaw,
  '/graphs/stator-current/:id?/': StatorCurrent,
  '/graphs/stator-temperature/:id?/': StatorTemperature,
  '/graphs/tip-speed-ratio/:id?/': TipSpeedRatio,

  '/graphs/curve-comparison/': CurveComparison,

  '/data-analysis/per-second/': PerSecond,
  '/data-analysis/per-minute/': PerMinute,
  '/data-analysis/query/': Query,

  '*': NotFound,
};

export default routes;
