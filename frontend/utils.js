import axios from 'axios';
import { format, formatDistance } from 'date-fns';
import { notify } from './stores.js';

export const api = axios.create({
  baseURL: window.API_BASE_URL || 'http://localhost:5000/api/',
  headers: {
    'Content-type': 'application/json',
  },
});

export function formatDate(rawDate) {
  const parsed = Date.parse(rawDate);
  return format(parsed, 'dd MMM yyyy (HH:mm)');
}

export function formatDateSeconds(rawDate) {
  const parsed = Date.parse(rawDate);
  return format(parsed, 'dd MMM yyyy (HH:mm:ss)');
}

export function dateDistance(start, end) {
  const s = Date.parse(start);
  const e = Date.parse(end);
  return formatDistance(s, e);
}

export function handleError(error) {
  if (error.isAxiosError) {
    if (error.response) {
      if (error.response.data && error.response.data.message) {
        notify(error.response.data.message, 'error');
        return;
      }
    } else if (error.request) {
      console.error(error.toJSON());
    }
  } else {
    console.error(error);
  }

  notify('Something went wrong. Please try again later.', 'error');
}
