export const domainName = 'openwindlab.net';

export const AC_VOLTAGE_VALUES = {
  name: 'AC Voltage (at rectifier)',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'AC Voltage (V)',
  yUnits: 'V',
  yAxisKey: 'avg_norm_voltage',
};

export const AERODYNAMIC_COEF_VALUES = {
  name: 'Aerodynamic Coefficient',
  xTitle: 'Tip Speed Ratio (λ)',
  xAxisKey: 'bin',
  yTitle: 'Aerodynamic Coef (Cp)',
  yUnits: 'Cp',
  yAxisKey: 'avg_aero_dynamic_coef',
};

export const AIR_DENSITY_VALUES = {
  name: 'Air Density',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Air Density (kg/m3)',
  yUnits: 'kg/m3',
  yAxisKey: 'avg_air_density',
};

export const AMBIENT_TEMPERATURE_VALUES = {
  name: 'Ambient Temperature',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Ambient Temperature (°C)',
  yUnits: '°C',
  yAxisKey: 'avg_ambient_temp',
};

export const ANUAL_ENERGY_VALUES = {
  name: 'Anual Energy Production',
  xTitle: 'Mean wind speed (m/s)',
  xAxisKey: 'wind_speed',
  yTitle: 'Energy (kWh)',
  yUnits: 'kWh',
  yAxisKey: 'aep',
};

export const BAROMETRIC_PRESSURE_VALUES = {
  name: 'Barometric Pressure',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Barometric Pressure (hPa)',
  yUnits: 'hPa',
  yAxisKey: 'avg_bar_pressure',
};

export const FURLING_ANGLE_VALUES = {
  name: 'Furling Angle',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Angle (deg)',
  yUnits: 'deg',
  yAxisKey: 'avg_furling_angle',
};

export const POWER_COEF_VALUES = {
  name: 'Power Coefficient',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Power Coefficient (Cp)',
  yUnits: 'Cp',
  yAxisKey: 'power_coef',
};

export const POWER_CURVE_VALUES = {
  name: 'Power Curve',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Power (W)',
  yUnits: 'W',
  yAxisKey: 'avg_norm_power',
};

export const RELATIVE_HUMIDITY_VALUES = {
  name: 'Relative Humidity',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Relative Humidity (%)',
  yUnits: '%',
  yAxisKey: 'avg_humidity',
};

export const ROTOR_YAW_VALUES = {
  name: 'Rotor Yaw',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Rotor Yaw (deg)',
  yUnits: 'deg',
  yAxisKey: 'avg_rotor_yaw',
};

export const STATOR_CURRENT_VALUES = {
  name: 'Stator Current',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Stator Current (A)',
  yUnits: 'A',
  yAxisKey: 'avg_norm_current',
};

export const STATOR_TEMPERATURE_VALUES = {
  name: 'Stator Temperature',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Stator Temperature (°C)',
  yUnits: '°C',
  yAxisKey: 'avg_stator_temp',
};

export const TIP_SPEED_RATIO_VALUES = {
  name: 'Tip Speed Ratio',
  xTitle: 'Wind speed bin (m/s)',
  xAxisKey: 'bin',
  yTitle: 'Tip Speed Ratio (λ)',
  yUnits: 'λ',
  yAxisKey: 'avg_lamda',
};
