import { domainName } from './constants.js';
import { api, handleError } from './utils.js';

export async function getWindturbines() {
  try {
    // If this request is made from the project's domain, show only the public wind turbines
    const fromPublicDomain = window?.location?.hostname === domainName;
    const response = await api.get(
      `windturbines/?is_public=${fromPublicDomain}`
    );
    return response.data;
  } catch (error) {
    handleError(error);
  }
}
