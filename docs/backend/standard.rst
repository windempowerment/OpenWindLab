Standard
========

This package contains the backend code for interacting with the available standard tables.
It contains the models, the views and the serializers for each of the standard tables.


AnualEnergyView
---------------

GET ``/api/windturbines/<int:id>/anual-energy/``

Return a list with the raw data of the anual energy production for the selected
windturbine. Also, return the information for the selected windturbine.

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "aep": 802.44977,
            "percentage": 10.73167,
            "uaep": 94.29732,
            "wind_speed": 4
        },
    ],
    "windturbine": {
        "description": "NTUA Small Wind Turbine",
        "furling_a": null,
        "furling_b": null,
        "id": 1,
        "location": "NTUA Rooftop",
        "meas_end": "2012-02-28T00:00:01",
        "meas_start": "2011-02-28T00:11:18",
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "radius": 1.2,
        "resistance": 0.35,
        "torque_a": 2.7311,
        "torque_b": 0.0557,
        "yaw_a": null,
        "yaw_b": null,
        "zero_current_margin": 0.16
    }
  }


LamdaBinsView
-------------

GET ``/api/windturbines/<int:id>/lamda-bins/``

Return a list with the raw data of the lamda bins for the selected
windturbine. Also, return the information for the selected windturbine.

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "avg_actual_meas_wind_speed": 1.9665,
            "avg_actual_power": 0.00392,
            "avg_aero_dynamic_coef": 0.00037,
            "bin": 0.0,
            "bin_frequency": 67.03856,
            "bin_samples": 302783
        },
    ],
    "windturbine": {
        "description": "NTUA Small Wind Turbine",
        "furling_a": null,
        "furling_b": null,
        "id": 1,
        "location": "NTUA Rooftop",
        "meas_end": "2012-02-28T00:00:01",
        "meas_start": "2011-02-28T00:11:18",
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "radius": 1.2,
        "resistance": 0.35,
        "torque_a": 2.7311,
        "torque_b": 0.0557,
        "yaw_a": null,
        "yaw_b": null,
        "zero_current_margin": 0.16
    }
  }


LamdaBinsPerBinView
-------------------

GET ``/api/windturbines/<int:id>/lamda-bins/<float:bin>/``

Return a list with the raw data of the lamda bins for the selected windturbine
and the selected bin. Also, return the information for the selected windturbine.

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "avg_actual_meas_wind_speed": 1.959945,
            "avg_actual_power": 0.003866,
            "avg_aero_dynamic_coef": 0.000373,
            "bin": 0.0,
            "bin_frequency": 68.73122,
            "bin_samples": 302471
        },
    ],
    "windturbine": {
        "description": "NTUA Small Wind Turbine",
        "furling_a": null,
        "furling_b": null,
        "id": 1,
        "location": "NTUA Rooftop",
        "meas_end": "2012-02-28T00:00:01",
        "meas_start": "2011-02-28T00:11:18",
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "radius": 1.2,
        "resistance": 0.35,
        "torque_a": 2.7311,
        "torque_b": 0.0557,
        "yaw_a": null,
        "yaw_b": null,
        "zero_current_margin": 0.16
    }
  }


MeasurementView
---------------

POST ``/api/windturbines/<int:id>/measurements/``

You send the ids of the wind turbines that you want to compare to the coresponding
API url and it returns the fields you need to plot the curve for each wind turbine.

**Query params:**

.. code-block:: json

  {
    "page": 1
  }

**Payload:**

.. code-block:: json

  [
    {
      "field": "avg_current",
      "op": ">",
      "value": 2
    },
  ]

**Response:**

.. code-block:: json



PerMinutesView
--------------

POST ``/api/windturbines/<int:id>/per-minutes/``

**Query params:**

.. code-block:: json

  {
    "page": 1
  }

**Payload:**

.. code-block:: json

  [
    {
      "field": "avg_current",
      "op": ">",
      "value": 2
    },
  ]

**Response:**

.. code-block:: json


PowerCurveView
--------------

GET ``/api/windturbines/<int:id>/power-curve/``

Return a list with the raw data of the power curve for the selected
windturbine. Also, return the information for the selected windturbine.

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "avg_air_density": 1.18537,
            "avg_ambient_temp": 16.86891,
            "avg_bar_pressure": 989.21338,
            "avg_furling_angle": 0.0,
            "avg_humidity": 0.50843,
            "avg_lamda": 2.47004,
            "avg_norm_current": 0.05326,
            "avg_norm_power": 3.98724,
            "avg_norm_voltage": 1.72594,
            "avg_norm_wind_speed": 0.43104,
            "avg_rotor_yaw": 101.94633,
            "avg_stator_temp": null,
            "avg_yaw_angle": 0.0,
            "bin": 0.5,
            "bin_samples": 60513,
            "max_avg_norm_power": 5.18882,
            "min_avg_norm_power": 2.78566,
            "power_coef": 17.96834,
            "si": 0.10995,
            "uc": 1.20158,
            "ui": 1.19654
        },
    ],
    "windturbine": {
        "description": "NTUA Small Wind Turbine",
        "furling_a": null,
        "furling_b": null,
        "id": 1,
        "location": "NTUA Rooftop",
        "meas_end": "2012-02-28T00:00:01",
        "meas_start": "2011-02-28T00:11:18",
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "radius": 1.2,
        "resistance": 0.35,
        "torque_a": 2.7311,
        "torque_b": 0.0557,
        "yaw_a": null,
        "yaw_b": null,
        "zero_current_margin": 0.16
    }
  }


ProcessedSecondView
-------------------

POST ``/api/windturbines/<int:id>/processed-seconds/``

**Query params:**

.. code-block:: json

  {
    "page": 1
  }

**Payload:**

.. code-block:: json

  [
    {
      "field": "avg_current",
      "op": ">",
      "value": 2
    },
  ]

**Response:**

.. code-block:: json


ShadowingRangeView
------------------

GET ``/api/windturbines/<int:id>/shadowing-range/``

Return a list with the raw data of the shadowing range for the selected
windturbine. Also, return the information for the selected windturbine.

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "range_end": 270.0,
            "range_start": 260.0
        }
    ],
    "windturbine": {
        "description": "NTUA Small Wind Turbine",
        "furling_a": null,
        "furling_b": null,
        "id": 1,
        "location": "NTUA Rooftop",
        "meas_end": "2012-02-28T00:00:01",
        "meas_start": "2011-02-28T00:11:18",
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "radius": 1.2,
        "resistance": 0.35,
        "torque_a": 2.7311,
        "torque_b": 0.0557,
        "yaw_a": null,
        "yaw_b": null,
        "zero_current_margin": 0.16
    }
  }
