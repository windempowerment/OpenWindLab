Backend
=======

The backend is a Flask application. The packages are separated into logical components
based on the functionality they are responsible for implementing.


Packages
--------

.. toctree::
  :maxdepth: 1

  windturbines
  standard
  curve_comparison
  data_analysis
