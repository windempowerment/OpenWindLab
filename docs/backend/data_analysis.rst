Data Analysis
=============

This package contains the backend code for interacting with the data analysis.
It contains the views and the serializers for per minute and per second analysis.


PerMinuteAnalysisView
---------------------

POST ``/api/data-analysis/per-minute/``

You send the id of the wind turbine that you want to analyze, the starting and ending datetimes
(the max range is 7 days) and the axis keys that you want to plot. It returns the data between
the requested datetimes and returns only the fields for the axis that you requested. The second
Y axis is not required.

**Payload:**

.. code-block:: json

  {
    "id": 8,
    "start_date": "2016-07-01 16:59",
    "end_date": "2016-07-06 16:59",
    "x_axis": "meas_datetime",
    "y_axis": "avg_mean_voltage",
    "y2_axis": "avg_vapour_pressure"
  }

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "meas_datetime": "2016-07-01T16:59:00",
            "avg_mean_voltage": 41.58258,
            "avg_vapour_pressure": 3856.75952
        },
    ]
  }


PerSecondAnalysisView
---------------------

POST ``/api/data-analysis/per-second/``

You send the id of the wind turbine that you want to analyze, the starting and ending datetimes
(the max range is 12 hours) and the axis keys that you want to plot. It returns the data between
the requested datetimes and returns only the fields for the axis that you requested. The second
Y axis is not required.

**Payload:**

.. code-block:: json

  {
    "id": 8,
    "start_date": "2016-07-01 04:59",
    "end_date": "2016-07-01 16:59",
    "x_axis": "rpm",
    "y_axis": "power_sec",
    "y2_axis": "humidity"
  }

**Response:**

.. code-block:: json

  {
    "raw_data": [
        {
            "rpm": 0.0,
            "power_sec": 0.03127,
            "humidity": 0.41657
        },
    ]
  }
