Curve Comparison
================

This package contains the backend code for interacting with the curve comparison.
It contains the views and the serializers for the curve comparison functionallity.
You send the ids of the wind turbines that you want to compare to the coresponding
API url and it returns the fields you need to plot the curve for each wind turbine.


AerodynamicCoefComparisonView
-----------------------------

POST ``/api/curve-comparison/aerodynamic-coef/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "avg_aero_dynamic_coef": 0.00037,
            "bin": 0.0
        },
    ],
    "2": [
        {
            "avg_aero_dynamic_coef": 0.00049,
            "bin": 0.0
        },
    ]
  }


AnualEnergyComparisonView
-------------------------

POST ``/api/curve-comparison/anual-energy/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "aep": 802.44977,
            "wind_speed": 4
        },
    ],
    "2": [
        {
            "aep": 751.61831,
            "wind_speed": 4
        },
    ]
  }


FurlingAngleComparisonView
--------------------------

POST ``/api/curve-comparison/furling-angle/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "avg_furling_angle": 0.0,
            "bin": 0.5
        },
    ],
    "2": [
        {
            "avg_furling_angle": 0.0,
            "bin": 2.0
        },
    ]
  }


PowerCoefComparisonView
-----------------------

POST ``/api/curve-comparison/power-coef/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "bin": 0.5,
            "power_coef": 17.96834
        },
    ],
    "2": [
        {
            "bin": 2.0,
            "power_coef": 0.01025
        },
    ]
  }


PowerCurveComparisonView
------------------------

POST ``/api/curve-comparison/power-curve/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "avg_norm_power": 3.98724,
            "bin": 0.5
        },
    ],
    "2": [
        {
            "avg_norm_power": 0.24778,
            "bin": 2.0
        },
    ]
  }


RotorYawComparisonView
----------------------

POST ``/api/curve-comparison/rotor-yaw/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "avg_rotor_yaw": 101.94633,
            "bin": 0.5
        },
    ],
    "2": [
        {
            "avg_rotor_yaw": 67.0808,
            "bin": 2.0
        },
    ]
  }


YawAngleComparisonView
----------------------

POST ``/api/curve-comparison/yaw-angle/``

**Payload:**

.. code-block:: json

  {
    "ids": [1, 2]
  }

**Response:**

.. code-block:: json

  {
    "1": [
        {
            "avg_yaw_angle": 0.0,
            "bin": 0.5
        },
    ],
    "2": [
        {
            "avg_yaw_angle": 0.0,
            "bin": 2.0
        },
    ]
  }
