Windturbines
============

This package contains the backend code for interacting with the available wind turbines.
It contains the model, the views and the serializers for the wind turbines.


WindTurbineListView
-------------------

Returns the list of all available wind turbines.

GET ``/api/windturbines/``

**Response:**

.. code-block:: json

  [
    {
        "id": 1,
        "name": "(SWT 1) 2.4m rotor diameter / grid connected",
        "description": "NTUA Small Wind Turbine",
        "location": "NTUA Rooftop",
        "resistance": 0.35,
        "zero_current_margin": 0.16,
        "radius": 1.2,
        "meas_start": "2011-02-28T00:11:18",
        "meas_end": "2012-02-28T00:00:01",
        "furling_a": null,
        "furling_b": null,
        "yaw_a": null,
        "yaw_b": null,
        "torque_a": 2.7311,
        "torque_b": 0.0557
    },
  ]


WindTurbineView
---------------

Returns the information about the requested wind tubine, based on the ``id`` given
in the url.

GET ``/api/windturbines/<int:id>/``

**Response:**

.. code-block:: json

  {
    "id": 1,
    "name": "(SWT 1) 2.4m rotor diameter / grid connected",
    "description": "NTUA Small Wind Turbine",
    "location": "NTUA Rooftop",
    "resistance": 0.35,
    "zero_current_margin": 0.16,
    "radius": 1.2,
    "meas_start": "2011-02-28T00:11:18",
    "meas_end": "2012-02-28T00:00:01",
    "furling_a": null,
    "furling_b": null,
    "yaw_a": null,
    "yaw_b": null,
    "torque_a": 2.7311,
    "torque_b": 0.0557
  }
