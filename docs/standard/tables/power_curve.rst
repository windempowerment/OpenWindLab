Power Curve
===========

The PowerCurve table stores the results regarding the SWT power curve (output table).


.. list-table::
  :widths: 2 2 5 3
  :header-rows: 1
  :stub-columns: 1

  * - Column
    - Type
    - Description
    - Notes
  * - WT_ID
    - smallint
    - unique id of the wind turbine
    - Composite Primary Key
  * - bin
    - decimal(18,5)
    - The wind speed bin
    - Composite Primary Key
  * - AvgNormWindSpeed
    - decimal(18,5)
    - Average normailized wind speed for the bin
    - m/sec
  * - AvgNormPower
    - decimal(18,5)
    - Average normailized power for the bin
    - W
  * - PowerCoef
    - decimal(18,5)
    - Power coefficient of the bin
    -
  * - Si
    - decimal(18,5)
    -
    -
  * - Ui
    - decimal(18,5)
    - Uncertainty
    -
  * - Uc
    - decimal(18,5)
    - Uncertainty
    -
  * - MinAvgNormPower
    - decimal(18,5)
    - Minimum Average normalized power of the bin
    - W
  * - MaxAvgNormPower
    - decimal(18,5)
    - Max Average normalized power of the bin
    - W
  * - AvgFurlingAngle
    - decimal(18,5)
    - Average furling angle of the bin
    - deg
  * - AvgYawAngle
    - decimal(18,5)
    - Average yaw angle of the bin
    - Deg
  * - AvgRotorYaw
    - decimal(18,5)
    - Average rotor yaw
    - Deg
  * - AvgStatorTemp
    - decimal(18,5)
    - Average stator temperature
    - Celsius
  * - AvgAmbientTemp
    - decimal(18,5)
    - Average ambient temperature
    - Celsius
  * - AvgLamda
    - decimal(18,5)
    - Average lamda of the bin
    -
