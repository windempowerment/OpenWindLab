Shadowing Range
===============

The ShadowingRange table is used to hold potential ranges (directions) that the SWT
production is hindered by an obstruction. The data is entered per SWT in ranges
(e.g. from 120 deg. To 160 deg. the wind is blocked by an obstacle). This is taken into
account when calculating the output.


.. list-table::
  :widths: 2 2 5 3
  :header-rows: 1
  :stub-columns: 1

  * - Column
    - Type
    - Description
    - Notes
  * - WT_ID
    - smallint
    - unique id of the wind turbine
    -
  * - Range_Start
    - decimal(18,5)
    - Starting degrees of the obstruction
    - Deg
  * - Range_End
    - decimal(18,5)
    - Ending degrees of the obstruction
    - Deg
