AEP
===

The AEP table holds data that produces the corresponding AEP table of the standard (output table)


.. list-table::
  :widths: 2 2 5 5
  :header-rows: 1
  :stub-columns: 1

  * - Column
    - Type
    - Description
    - Notes
  * - WT_ID
    - smallint
    - unique id of the wind turbine
    - Composite Primary Key
  * - WindSpeed
    - Int
    - Wind speed
    - Composite Primary Key
  * - AEP
    - decimal(18,5)
    - AEP
    - ?
  * - Uaep
    - decimal(18,5)
    - Uncertainty of production
    - ?
  * - Percentage
    - decimal(18,5)
    - %
    - %
