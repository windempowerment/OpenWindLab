Winturbine
==========

It holds information about the wind turbine being measured. The columns are:


.. list-table::
    :widths: 2 2 6 5 1
    :header-rows: 1
    :stub-columns: 1

    * - Column
      - Type
      - Description
      - Notes
      - Required
    * - WT_ID
      - smallint
      - unique id of the wind turbine
      - Primary Key
      - Y
    * - WT_Name
      - nvarchar(100)
      - friendly short name of the SWT
      -
      - Y
    * - WT_Description
      - nvarchar(255)
      - description of the SWT
      -
      - N
    * - Location
      - nvarchar(50)
      - Location of the SWT
      -
      - Y
    * - Resistance
      - decimal(18,5)
      - any resistance introduced by the cabling from the SWT to the metering equipment
      - Ω
      - Y
    * - ZeroCurrentMargin
      - decimal(18,5)
      - the threshold where the current is considered 0
      - A
      - Y
    * - Radius
      - decimal(18,5)
      - the radius of the SWT
      - Ω
      - Y
    * - Meas_Start
      - datetime2
      - The first date of the available data for the specified SWT
      - Auto calculated by the system when the Power Curve calculations is requested
      - N
    * - Meas_End
      - datetime2
      - The first date of the available data for the specified SWT
      - Auto calculated by the system when the Power Curve calculations is requested
      - N
    * - Furling_a
      - decimal(18,5)
      - Coefficient a for calculating the furling angle using the f(x) = a*x + b function
      - x is the voltage reported by the sensor
      - N
    * - Furling_b
      - decimal(18,5)
      - If information was provided in the input files about the furling angles these are the values that are used to calculate the furling angle using the f(x) = a*x + b function
      - x is the voltage reported by the sensor
      - N
    * - Yaw_a
      -
      - Coefficient to calculate the yaw angle using the f(x) = a*x + b function
      - x is the voltage reported by the sensor
      - N
    * - Yaw_b
      -
      - Coefficient to calculate the yaw angle using the f(x) = a*x + b function
      - x is the voltage reported by the sensor
      - N
    * - Torque_a
      -
      - Coefficient used to calculate the torque f(x) = a*x + b
      - x is the voltage reported by the sensor
      - N
    * - Torque_b
      -
      - Coefficient used to calculate the torque f(x) = a*x + b
      - x is the voltage reported by the sensor
      - N
