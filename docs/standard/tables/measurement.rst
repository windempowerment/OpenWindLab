Measurement
===========

The Measurement table holds the raw data as inserted from the parser program. The fields are :


.. list-table::
    :widths: 2 2 5 3 1
    :header-rows: 1
    :stub-columns: 1

    * - Column
      - Type
      - Description
      - Notes
      - Required
    * - WT_ID
      - smallint
      - unique id of the wind turbine
      - Composite key with MeasTimeStamp
      - Y
    * - MeasTimeStamp
      - datetime2
      - timestamp of the measurement
      - Composite key with WT_ID
      - Y
    * - WindDirection
      - decimal(18,5)
      - direction of the wind
      - deg.
      - Y
    * - WindSpeed
      - decimal(18,5)
      - wind speed
      - m/sec
      - Y
    * - ValidationWindSpeed
      - decimal(18,5)
      - wind speed that is used to validate the wind speed sensor
      - m/sec
      - N
    * - Temperature
      - decimal(18,5)
      - temperature
      - Celsius
      - Y
    * - Humidity
      - decimal(18,5)
      - humidity
      - %Re
      - Y
    * - BarPressure
      - decimal(18,5)
      - atmospheric pressure
      - hPa
      - Y
    * - CurrentR
      - decimal(18,5)
      - AC Current produced by the SWT (Phase 1)
      - A
      - Y
    * - CurrentS
      - decimal(18,5)
      - AC Current produced by the SWT (Phase 2)
      - A
      - Y
    * - CurrentT
      - decimal(18,5)
      - AC Current produced by the SWT (Phase 3)
      - A
      - Y
    * - VoltageR
      - decimal(18,5)
      - AC Voltage produced by the SWT (Phase 1)
      - V
      - Y
    * - VoltageS
      - decimal(18,5)
      - AC Voltage produced by the SWT (Phase 2)
      - V
      - Y
    * - VoltageT
      - decimal(18,5)
      - AC Voltage produced by the SWT (Phase 3)
      - V
      - Y
    * - PowerAC
      - decimal(18,5)
      - Power that the sensor reports
      - W
      - N
    * - RPM
      - decimal(18,5)
      - revolutions per minute
      -
      - Y
    * - FurlingAngleVoltage
      - decimal(18,5)
      - the voltage of the sensor that measures the furling angle
      - V
      - N
    * - YawAngleVoltage
      - decimal(18,5)
      - the voltage measured by the sensor of the yaw angle
      - V
      - N
    * - StatorTemp
      - decimal(18,5)
      - the temperature of the stator
      - Celsius
      - N
