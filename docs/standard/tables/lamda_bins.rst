Lamda Bins
==========

The LamdaBins bins the data by lamda instead of windspeed (output table)


.. list-table::
  :widths: 2 2 5 3
  :header-rows: 1
  :stub-columns: 1

  * - Column
    - Type
    - Description
    - Notes
  * - WT_ID
    - smallint
    - unique id of the wind turbine
    - Composite Primary Key
  * - bin
    - decimal(10,3)
    - Lamda bin
    - Composite Primary Key
  * - bin_Samples
    - decimal(18,5)
    - Number of samples for this bin
    -
  * - bin_Frequency
    - decimal(18,5)
    - Frequency of the bin
    -
  * - AvgActualMeasWindSpeed
    - decimal(18,5)
    - Average actual measured windspeed
    - m/s
  * - AvgActualPower
    - decimal(18,5)
    - Average actual power
    - W
  * - AvgAeroDynamicCoef
    - decimal(18,5)
    - Average aerodynamic coefficient
    -
