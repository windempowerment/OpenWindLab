Per minute
==========

This table stores per minute aggregations of the raw measurements (Measurements table)
for each turbine. The per minute calculation takes place as an intermediate step to the
overall standard tables calculation and the results are stored for speed and ease of future
querying the dataset. It consists of the fields :


.. list-table::
  :widths: 2 2 5 3
  :header-rows: 1
  :stub-columns: 1

  * - Column
    - Type
    - Description
    - Notes
  * - WT_ID
    - smallint
    - unique id of the wind turbine
    - Composite key with MeasTimeStamp
  * - MeasDateTime
    - datetime2
    - timestamp of the measurement
    - Composite key with WT_ID
  * - AvgCurrent
    - decimal(18,5)
    - Average current for the minute
    - A
  * - AvgCurrentSamples
    - Int
    - Number of samples that were present in that minute
    -
  * - AvgMeanVoltage
    - decimal(18,5)
    - Mean voltage for the minute
    - V
  * - AvgVoltageSamples
    - Int
    - Number of samples for the voltage present in the minute
    -
  * - AvgVapourPressure
    - decimal(18,5)
    - Average Vapor pressure for the minute
    -
  * - AvgAirDensity
    - decimal(18,5)
    - Average Air Density
    -
  * - AvgPower
    - decimal(18,5)
    - Average Power of the minute
    - W
  * - MaxPower
    - decimal(18,5)
    - Max power of the minute
    - W
  * - MinPower
    - decimal(18,5)
    - Min power of the minute
    - W
  * - AvgWindSpeed
    - decimal(18,5)
    - Average wind speed
    - m/sec
  * - PowerDeviation
    - decimal(18,5)
    - Max Min power deviation
    - m/sec
  * - AvgRPM
    - decimal(18,5)
    - Average RPM
    -
  * - Torque
    - decimal(18,5)
    - Torque
    -
  * - RadSpeed
    - decimal(18,5)
    - Radial speed
    -
  * - Pw
    - decimal(18,5)
    - ?
    -
  * - Lamda
    - decimal(18,5)
    - Lamda
    -
  * - Cp
    - decimal(18,5)
    - Aerodynamic coeff
    -
  * - AvgWindDirection
    - decimal(18,5)
    - Average wind direction
    - Deg
  * - AvgWindSpeed_NormSeaLevel
    - decimal(18,5)
    - Average wind speed normalized at sea level
    - m/sec
  * - AvgWindSpeed_Norm
    - decimal(18,5)
    - Normalized average speed
    - m/sec
  * - AvgPower_NormSeaLevel
    - decimal(18,5)
    - Average normalized power at sea level
    - W
  * - AvgPower_Norm
    - decimal(18,5)
    - Average normalized power
    - W
  * - MaxPower_NormSeaLevel
    - decimal(18,5)
    - Max power normalized power at sea level
    - W
  * - MaxPower_Norm
    - decimal(18,5)
    - Max power normalized
    - W
  * - MinPower_NormSeaLevel
    - decimal(18,5)
    - Min power normalized power at sea level
    - W
  * - MinPower_Norm
    - decimal(18,5)
    - Min power normalized
    - W
  * - PowerDeviation_NormSeaLevel
    - decimal(18,5)
    - Max min power deviation normalized at sea level
    - W
  * - PowerDeviation_Norm
    - decimal(18,5)
    - Normalized power deviation
    -
  * - AvgFurlingAngle
    - decimal(18,5)
    - Average furling angle
    - Deg
  * - AvgYawAngle
    - decimal(18,5)
    - Average yaw angle
    - Deg
  * - AvgRotorYaw
    - decimal(18,5)
    - Average rotor yaw
    - ?
  * - AvgStatorTemp
    - decimal(18,5)
    - Average stator temp
    - Celsius
