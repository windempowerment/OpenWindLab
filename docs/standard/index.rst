
Database
========

The core database schema is shown below. It consists of 7 tables:

.. image:: ../images/schema.png
  :width: 800
  :alt: Database schema


Implementation
--------------

The implementation of the standard is done exclusively using T-SQL (in SQL Server 2008)
provided that the correct information is inserted in the Measurement table per second and
using the correct units per column. It uses pre-defined structure tab delimited files that
are parsed and inserted in the database which calculates the values (AEP, Power Curve etc.)
and populates the corresponding tables. These tables can be then queried to get the outcome
of the calculations.


Calculation steps
-----------------

Once the data have been stored in the Measurement tables the calculation is started by calling
the SQL stored procedure ``sp_AllCalculationsPerMinute``.

The calculation process starts by calling the SQL Server stored procedure ``sp_AllCalculationsPerMinute``
(giving the desired options described above) which populates the output tables
(AEP, PowerCurve and LamdaBins).

#. All the output tables are deleted for the WT_ID so that the new data are inserted
#. If the user selected to recalculate the per minute aggregates the stored procedure deletes
   all the rows in the PerMinutes table that belong to WT_ID
#. Calls ``sp_InsertMeanCalculations`` which calculates the per minute aggregates that are then
   inserted into the PerMinutes table. (this stored procedure in turn calls the user defined
   procedures udf_MeanPerSecond and udf_MeanPerMinute that filter and aggregate the data)
#. When the values are inserted into the PerMinutes table the second flow is called using the
   ``sp_InserIECTables`` stored procedure. It calculates the tables that are required by the
   standard and inserts the values into the output data tables. (in conjunction with the user
   defined function udf_ByBin table that creates the bins).
#. Last step is calculating the lamda bins by using the user defined function ``udf_byLamdaBin``
   and inserting the result into the LambaBins table.


Usage
-----

The user provides a pre-determined file structure (tab delimited text file) that contains rows
with the captured data. Each row represents the data taken in a given second. A sample of this
file is shown below.

.. image:: ../images/sample_file.png
  :width: 800
  :alt: Data sample file


The fields provided in the above file are:

#. Date in YYYYMMdd format
#. Time : in hhmmss format
#. Temperature (Celsius)
#. Humidity (%Re)
#. Pressure in (hPa)
#. Wind speed (m/sec)
#. Validation wind speed (m/sec)
#. Wind Direction (deg)
#. AC Power (in Watts)
#. AC Current R (A)
#. AC Current S (A)
#. AC Current T (A)
#. AC Voltage R (V)
#. AC Voltage S (V)
#. AC Voltage T (V)
#. DC Current (I) – not taken into account
#. DC Voltage (V) – not taken into account
#. RPM

The file is then fed into a .NET windows application that parses, validates and transforms the
data as needed and inserts them into the SQL Server Database. The above file structure is one of
many that are accepted (minor changes to the order and number of the columns). Each format has
its own parser and validation that handles the data insertion.

.. image:: ../images/net_app.png
  :width: 800
  :alt: Dot NET application


SQL Procedures
--------------

.. code-block:: sql

  sp_AllCalculationsPerMinute:

  -- Wind turbine ID to run calculations for
  @WTID as smallint

  -- Delete previous calculations per minute before calculating ? Default true
  @ReCalculateMinutes bit = 1

  -- Start calculating the minutes of the SWT from this date forth.Default a day from the past so all the measurements
  @StartDate datetime2 = '2009-01-01'

  -- Stop using measurements that are past this date. Default is now so all the measurements
  @EndDate datetime2 = GETDATE

  -- The bin width to use. Standard dictates 0.5 so this is the default if it is not provided
  @binWidth decimal(18,5) = 0.5

  -- Calculations are made for the sea level? Defaults to true
  @SeaLevel bit = 1

  -- If the wind is blocked from some direction and the user has inserted those ranges in the ShadowingRange table this should be set to true? Defaults to true
  @ShadowingON bit=1


Tables
------

.. toctree::
  :maxdepth: 1

  tables/windturbine
  tables/measurement
  tables/per_minute
  tables/power_curve
  tables/aep
  tables/lamda_bins
  tables/shadowing_range
