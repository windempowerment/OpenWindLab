# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import sphinx_wagtail_theme

from datetime import datetime

# -- Project information -----------------------------------------------------

project = "OpenWindLab"
author = "Rural Electrification Research Group"
copyright = "{year:d}, OpenWindLab NTUA".format(year=datetime.now().year)

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_wagtail_theme",
]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_wagtail_theme"

html_theme_options = dict(
    project_name="OpenWindLab",
    logo="img/logo.png",
    logo_alt="OpenWindLab logo",
    logo_height=59,
    logo_url="/",
    logo_width=45,
    footer_links=",".join(
        [
            "About Us|https://openwindlab.net/#about/",
            "RurERG|https://rurerg.net/",
            "Smart Rue|https://www.smartrue.gr/en/",
        ]
    ),
)

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["static"]

# Add any relative paths that contain templates.
templates_path = ["_templates"]
