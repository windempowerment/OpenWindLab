Stack overview
==============

A web application built with `Python`_ on the server-side and `Javascript`_ on the client-side.

.. _`Python`: https://www.python.org/
.. _`JavaScript`: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference


Backend
-------

The backend is a `Flask`_ application. It is responsible for:

* Interfacing with the database, using `Flask-SQLAlchemy`_
* Routing and handling of API calls, using `FlaskRESTful`_
* Object serialization/deserialization, using `Flask-Marshmallow`_
* Serving static assets such as js, css and images

.. _`Flask`: https://flask.palletsprojects.com/
.. _`Flask-SQLAlchemy`: https://flask-sqlalchemy.palletsprojects.com/
.. _`FlaskRESTful`: https://flask-restful.readthedocs.io/en/latest/
.. _`Flask-Marshmallow`: https://flask-marshmallow.readthedocs.io/en/latest/


Frontend
--------

The frontend is a `Svelte`_ single page application. It is responsible for:

* Styling the HTML, using `Tailwind CSS`_
* Top-level hash based routing, using `svelte-spa-router`_
* Creating interactive charts, using `chartjs`_
* Interacting with the server through the API, using `Axios`_

.. _`Svelte`: https://svelte.dev/
.. _`Tailwind CSS`: https://tailwindcss.com/
.. _`svelte-spa-router`: https://github.com/ItalyPaleAle/svelte-spa-router
.. _`chartjs`: https://www.chartjs.org/
.. _`Axios`: https://axios-http.com/


Documentation
-------------

* Formatted using `reStructuredText`_
* The output is compiled by `Sphinx`_
* Theme used `sphinx-wagtail-theme`_

.. _`reStructuredText`: http://docutils.sourceforge.net/rst.html
.. _`Sphinx`: http://www.sphinx-doc.org/en/stable/rest.html
.. _`sphinx-wagtail-theme`: https://sphinx-wagtail-theme.readthedocs.io/en/latest/index.html
