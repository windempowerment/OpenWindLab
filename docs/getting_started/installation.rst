Installation
============

Most of the steps below require entering commands into your terminal.

.. note::
  The following steps have been tested on a Linux operating system, so if you're running
  Windows, you are likely to encounter issues with this guide. That said, contributions to
  improve support for Windows are welcome!


Requirements
------------

If you are familiar with Docker, you can set up the project for development without setting
up the full JavaScript and Python development environments. All you need is:

* `Docker`_ and `docker-compose`_

.. _`Docker`: https://docs.docker.com/get-docker/
.. _`docker-compose`: https://docs.docker.com/compose/install/


Otherwise, in order for you to build and run the source code locally, you should have some
the following tools installed:

* `Node.js`_ v18+ and `npm`_
* `Python`_ v3.11+ and `pip`_

.. _`Node.js`: https://nodejs.org/en/download/
.. _`npm`: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
.. _`Python`: https://www.python.org/downloads/
.. _`pip`: https://pip.pypa.io/en/stable/installing/


Git and GitLab
--------------

#. Install and set up `Git`_ on your computer
#. Sign up and configure your `GitLab account`_ if you don't have one already
#. Fork the `project repository`_. This will make it easier to submit pull requests

.. tip::
  `Register your SSH keys`_ on GitLab to avoid having to repeatedly enter your password

.. _`Git`: https://docs.gitlab.com/ee/topics/git/how_to_install_git/
.. _`GitLab account`: https://gitlab.com/users/sign_up/
.. _`project repository`: https://gitlab.com/rurerg/openwindlab
.. _`Register your SSH keys`: https://docs.gitlab.com/ee/ssh/


Clone the project to your computer. In the command below, replace ``$USERNAME`` with your
own GitLab username:

.. code:: console

  $ git clone git@gitlab.com:$USERNAME/openwindlab.git
  $ cd windlab


Run the project using Docker
----------------------------

To run the project using Docker, run the following commands:

.. code:: console

  $ make build
  $ make up


Run the project locally
-----------------------

To run the project locally, run the following commands:

.. code:: console

  $ make install
  $ pipenv shell
  (openwindlab)$ npm run devserver

The ``make install`` command initializes and builds everything:

#. Initializes a virtual environment
#. Installs the python dependencies
#. Installs the npm packages
#. Builds the frontend assets
#. Builds the documentation

The ``pipenv shell`` command spawns a shell within the virtual environment.

The ``npm run devserver`` command starts the frontend and backend servers in parallel.
The following will be available:

* Frontend @ http://localhost:3000
* Backend @ http://localhost:5000

Open http://localhost:5000 on your web browser and happy coding :)
