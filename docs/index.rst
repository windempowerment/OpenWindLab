Welcome to OpenWindLab's documentation!
=======================================

Small wind turbine data analysis tool ~ OpenWindLab, NTUA


What is this tool?
------------------

OpenWindLab is an open source tool developed by the Rural Electrification Research Group,
that calculates the performance characteristics of small wind turbines based on electrical
and meteorological measurements, according to IEC 61400-12-1. The power curve, the power
coefficient and the annual energy production are calculated and presented. The data can be
displayed in per second raw values or per minute averages. All data can be plotted for
different combinations of variables and custom made queries can be made to the data base
with regard to all data variables.


.. toctree::
  :maxdepth: 2

  getting_started/installation
  getting_started/overview
  standard/index
  backend/index
