VENV = $(shell pipenv --venv)

# Put it first so that "make" without argument is like "make help".
.PHONY: help
help:
	@echo "Please use \`make target' where target is one of"
	@echo "  deploy           Rebuild the web container and deploy the latest changes"
	@echo "  assets           Install and build static assets"
	@echo "  clean            Remove virtual env and files not in source control"
	@echo "  image            Build the production image locally and start the detached container"
	@echo "  build            Build the development containers"
	@echo "  up               Start the development containers"
	@echo "  down             Stop all running development containers"
	@echo "  logs             Output the flask container logs"
	@echo "  docs             Compile docs"
	@echo "  help             Show this help message"
	@echo "  install          Install project dependencies"
	@echo "  python           Install python dependencies"
	@echo "  todo             Look for areas of the code that need updating"
	@echo "  venv             Create the virtual environment"


# --- Prod commands

.PHONY: deploy
deploy:
	git pull --ff-only origin main
	sudo cp nginx/openwindlab.net /data/vhost.d/openwindlab.net
	sudo cp nginx/www.openwindlab.net /data/vhost.d/www.openwindlab.net
	sudo cp nginx/demo.openwindlab.net /data/vhost.d/demo.openwindlab.net
	docker-compose up --detach --build
	docker cp owl-web:/home/nonroot/app/static/build app/static/
	docker cp app/static nginx-proxy:/app/
	docker exec nginx-proxy nginx -s reload
	rm -rf app/static/build


# --- Install commands

.PHONY: install
install: venv python assets docs

.PHONY: venv
venv:
	pip3 install pipenv
	pipenv shell --python 3.11 | exit

.PHONY: python
python:
	pipenv install --dev

.PHONY: assets
assets:
	npm install


# --- Docker commands

.PHONY: image
image:
	docker build -t openwindlab:latest .
	docker run --name openwindlab -p 5000:5000 --env-file .env --detach openwindlab:latest
	docker system prune --force

.PHONY: build
build:
	docker-compose --file docker-compose.dev.yml build

.PHONY: up
up:
	docker-compose --file docker-compose.dev.yml up

.PHONY: down
down:
	docker-compose --file docker-compose.dev.yml stop
	docker system prune --force

.PHONY: logs
logs:
	docker-compose logs --follow --tail 30


# --- Project Management

.PHONY: docs
docs:
	make -C docs html SPHINXBUILD=$(VENV)/bin/sphinx-build

.PHONY: clean
clean:
	find . -type f -name "*.pyc" -delete
	-pipenv --rm
	-rm -rf app/static/build/
	-rm -rf node_modules/
	-rm -rf docs/_build/

.PHONY: todo
todo:
	-grep -rnH TODO --include="*.py" app/
	-grep -rnH TODO --include="*.rst" docs/
	-grep -rnH TODO --include=\*.{svelte,js} frontend/
